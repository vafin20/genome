import psycopg2

conn = psycopg2.connect(dbname='postgres', user='postgres',
                        password='12345678', host='database-1.ctkr7egrrhb0.us-east-1.rds.amazonaws.com')
print("connected to database")
cursor = conn.cursor()

cursor.execute("with _intersection as (select * from dna1k3 intersect all select * from dna2k3),_unioning as (select * from dna1k3 union all select * from dna2k3) "
               "select ((select count(*) from _intersection)::float)/(select count(*) from _unioning);")
jaccard = cursor.fetchall()
print(jaccard)

cursor.execute("with _intersection as (select * from dna1k5 intersect all select * from dna2k5),_unioning as (select * from dna1k5 union all select * from dna2k5) "
               "select ((select count(*) from _intersection)::float)/(select count(*) from _unioning);")
jaccard = cursor.fetchall()
print(jaccard)

cursor.execute("with _intersection as (select * from dna1k9 intersect all select * from dna2k9), _unioning as (select * from dna1k9 union all select * from dna2k9) "
               "select ((select count(*) from _intersection)::float)/(select count(*) from _unioning);")
jaccard = cursor.fetchall()
print(jaccard)

print("Finished")
