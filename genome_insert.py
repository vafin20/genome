import psycopg2

conn = psycopg2.connect(dbname='postgres', user='postgres',
                        password='12345678', host='database-1.ctkr7egrrhb0.us-east-1.rds.amazonaws.com')
print("connected to database")
cursor = conn.cursor()

f1 = open('Genome_1-1.txt', 'r')
dna1 = f1.read().replace("\n", "")
# table dna1k3
f2 = open('Genome_2-1.txt', 'r')
dna2 = f2.read().replace("\n", "")
# table dna2k3


# sql injection vulnerable code below
# haven't found right way to do this with psycopg12 lib
dna1k3 = ""
for x in range(0, len(dna1) - 3):
    dna1k3 += "(\'"
    dna1k3 += dna1[x:x+3] +"\'), "
print(dna1k3)
dna1k3 = dna1k3[:-2]
SQL = "insert into dna1k3 (dna) values" + dna1k3 + ";"
cursor.execute(SQL)

dna1k5 = ""
for x in range(0, len(dna1) - 5):
    dna1k5 += "(\'"
    dna1k5 += dna1[x:x+5] +"\'), "
dna1k5 = dna1k5[:-2]
print(dna1k5)
SQL = "insert into dna1k5 (dna) values" + dna1k5 + ";"
cursor.execute(SQL)

dna1k9 = ""
for x in range(0, len(dna1) - 9):
    dna1k9 += "(\'"
    dna1k9 += dna1[x:x+9] +"\'), "
print(dna1k9)
dna1k9 = dna1k9[:-2]
SQL = "insert into dna1k9 (dna) values" + dna1k9 + ";"
cursor.execute(SQL)

dna2k3 = ""
for x in range(0, len(dna2) - 3):
    dna2k3 += "(\'"
    dna2k3 += dna2[x:x+3] +"\'), "
dna2k3 = dna2k3[:-2]
SQL = "insert into dna2k3 (dna) values" + dna2k3 + ";"
cursor.execute(SQL)

dna2k5 = ""
for x in range(0, len(dna2) - 5):
    dna2k5 += "(\'"
    dna2k5 += dna2[x:x+5] +"\'), "
dna2k5 = dna2k5[:-2]
SQL = "insert into dna2k5 (dna) values" + dna2k5 + ";"
cursor.execute(SQL)

dna2k9 = ""
for x in range(0, len(dna2) - 9):
    dna2k9 += "(\'"
    dna2k9 += dna2[x:x + 9  ] + "\'), "
dna2k9 = dna2k9[:-2]
SQL = "insert into dna2k9 (dna) values" + dna2k9 + ";"
cursor.execute(SQL)

conn.commit()
cursor.close()
conn.close()
